﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "End")
        {
            gameObject.SetActive(false);
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
